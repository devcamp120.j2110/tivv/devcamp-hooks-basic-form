import { useEffect, useState } from "react";
function HookForm() {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstName") ? localStorage.getItem("firstName") : "");
    const [lastName, setLastName] = useState(localStorage.getItem("lastName") ? localStorage.getItem("lastName") : "");

    const inputFirstName = (event) => {
        setFirstName(event.target.value)
    } 
    const inputLastName = (event) => {
        setLastName(event.target.value)
    }
    useEffect(() => {
        localStorage.clear();
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
    }) 
    return (
        <div>
            <label>First name:  </label>
            <input onChange={inputFirstName}/><br/>
            <label>Last name:  </label>
            <input onChange={inputLastName}/> 
            <hr/>
            <p>Value input: {firstName} {lastName}</p>
        </div>
    )
}
export default HookForm;